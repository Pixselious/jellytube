# JellyTube

JellyTube is going to be a Jellyfin plugin. The goal is to automate downloads from YouTube and extend Jellyfin's UI to better suite this type of content.

Another important goal is to make it easy for people to support their favourite creators without watching ads.
